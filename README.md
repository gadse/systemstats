# systemstats

A small utility to gather system stats (CPU load, memory usage, GPU load) in a lightweight manner.

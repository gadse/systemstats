extern crate sys_info;
extern crate fern;
extern crate log;
extern crate chrono;

use std::collections::LinkedList;

use sys_info::{mem_info, MemInfo, Error, LoadAvg, loadavg, os_type, os_release};
use log::{debug, info, trace, warn};

fn main() {
    let memory = mem_info();
    let load = loadavg();
    print_tldr(&memory, &load);
    println!();
    print_memory(&memory);
    println!();
    print_os_and_cpu_load(&load);
}


fn setup_logger() -> Result<(), fern::InitError> {
    /// Taken from fern docs
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}[{}][{}] {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .chain(fern::log_file("output.log")?)
        .apply()?;
    Ok(())
}


fn print_tldr(memory: &Result<MemInfo, Error>, load: &Result<LoadAvg, Error>) {
    match memory {
        Ok(mem) => println!("{:.2} GB used of {:.2} GB",
                            (mem.total - mem.avail) as f64 / 1000000.0,
                            (mem.total) as f64 / 1000000.0
        ),
        Err(mem) => {}
    }
    match load {
        Ok(ld) => println!("{:.2} CPU load", ld.one),
        Err(ld) => {}
    }
}


fn print_memory(memory: &Result<MemInfo, Error>) -> () {
    match memory {
        Ok(memory) => {
            let used = memory.total - memory.avail;
            let used_percent = used as f64 / memory.total as f64 * 100.0;
            println!("### MEMORY");
            println!("  % used |    MB used |   MB total | MB free");
            println!("---------+------------+------------+--------");
            println!("{:>7.2}% | {:>10} | {:>10} | {:>10}",
                format_nicely(&used_percent, None, None),
                format_nicely(&used, None, None),
                format_nicely(&memory.total, None, None),
                format_nicely(&memory.avail, None, None),
            )
        }
        Err(memory) => {
            println!("Error getting memory stats. Error:\n{}", memory)
        }
    }
}


fn print_os_and_cpu_load(load: &Result<LoadAvg, Error>) {
    let mut load_is_current = false;
    match os_type() {
        Ok(os) => {
            println!("### OPERATING SYSTEM");
            print!("{}", os);
            if os.to_lowercase().contains("windows") {
                load_is_current = true;
            }
        }
        Err(os)=> {
            println!("Error getting OS type. Error:\n{}", os)
        }
    }
    match os_release() {
        Ok(release) => println!(" ({})", release),
        Err(release) => println!("Error getting OS release. Error:\n{}", release)
    }
    println!();
    match load {
        Ok(load) => {
            if load_is_current {
                println!("### LOAD");
                println!("current: {:>5.2}", load.one);
            } else {
                println!("### LOAD");
                println!(" 1 min  | 5 min  | 15 min");
                println!("--------+--------+----------");
                println!("{:>7.2} |{:>7.2} |{:>7.2}",
                    load.one,
                    load.five,
                    load.fifteen
                )
            }
        }
        Err(load) => {
            println!("Error getting load stats. Error:\n{}", load)
        }
    }
}


fn format_nicely<T: ToString>(number: &T, custom_delimiter: Option<char>, custom_decimal: Option<char>) -> String {
    // TODO: Fractal part won't show...
    let representation: String = number.to_string();
    let split_number: Vec<&str> = representation.split(".").collect();
    let integer_part = split_number[0];

    let fractal_part: &str;
    if split_number.len() > 1 {
        fractal_part = split_number[1];
    } else {
        fractal_part = "";
    }
    debug!("NUMBER IS {}", number.to_string());
    debug!("FRACTAL PART IS {}", fractal_part);
    debug!("EMPTY? {}", fractal_part.is_empty());

    let mut delimiter: char = ' ';
    if let None = custom_delimiter {
        delimiter = ',';
    } else if let Some(del) = custom_delimiter {
        delimiter = del;
    }

    let mut decimal: char = ' ';
    if let None = custom_decimal {
        decimal = '.';
    } else if let Some(dec) = custom_decimal {
        decimal = dec;
    }

    let mut new_representation: LinkedList<char> = LinkedList::new();
    debug!("number={} ==> ", integer_part);
    for (ix, c) in integer_part.chars().rev().enumerate() {
        debug!("number[{}]={} ", ix, c);
        new_representation.push_front(c);
        if ix > 1 && (ix + 1) % 3 == 0 && ix < integer_part.len() - 1 {  // If we have digits left
            new_representation.push_front(delimiter);
            debug!("dot");
        }
    }

    if fractal_part.is_empty() == false {
        debug!("Adding fractal part!");
        new_representation.push_back(decimal);
        let formatted_integer_part = new_representation.iter().cloned().collect::<String>();
        return formatted_integer_part + fractal_part;
    } else {
        return new_representation.iter().cloned().collect::<String>();
    }
}

